Mosaik Photovoltaic Adapter
=======================================
An adapter to couple a model from the PVLib library to a Co-Simulation environment using the MOSAIK API

Package Installation
-----------------
pip install git+ssh://git@gitlab.com/zdin-zle/models/photovoltaic.git

The ssh key needs to be first configured for this to work, since it is a private repository.

To see about ssh configurations, visit https://docs.gitlab.com/ee/ssh/

Description
-----------------
The adapter utilizes an initial configuration file based on scenarios. Default configurations are included. Custom configurations can be passed if they have the same dictionary structure

The Mosaik API calls the model to build simulators. A model of a PV System, represented in the class PVSystem(), is built based on these configurations. 

The PVSystem() class includes methods from the PVLib library to calculate the output AC power based on the environmental parameters and configurations of modules, inverters, arrays, etc...

The Adapter couples this model with the inputs from environmental data (such as from a CSV reader) and calculates the output power for each step. The only output of the adapter so far is the Output AC Power.

The Mosaik API then requests advancing time, which is then requested to the simulator and then to the PVSystem(), which advances time internally. This modifies the internal time of the PVSystem() object so the right solar angles are considered for calculations. Then new meteorological data can be passed to calculate power output in the new time point.

Graphical Description
-----------------
![](docs/photovoltaik.jpg)