'''
Created on 18.05.2021

@author: Fernando Penaherrera @UOL/OFFIS

Dummy file for testing functionalities of the environment

'''
import os
import pvlib
a = 1
b = 2


def summ(a, b):
    return a + b


x = "hello"
x1 = 0


if __name__ == '__main__':
    print(summ(2, 4))
    print("A")
    print(os.getcwd())
    print(pvlib.__version__)
