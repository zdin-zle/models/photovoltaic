'''
Created on 13.12.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import pandas as pd
import pvlib
from pvlib.pvsystem import PVSystem
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS
from pvlib import pvsystem
from common import normalize_product_names

temperature_model_parameters = TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_glass']
sandia_modules = pvlib.pvsystem.retrieve_sam('SandiaMod')
cec_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
sandia_module = sandia_modules['Canadian_Solar_CS5P_220M___2009_']
cec_inverter = cec_inverters['ABB__MICRO_0_25_I_OUTD_US_208__208V_']

location = Location(latitude=32.2, longitude=-110.9)
system = PVSystem(surface_tilt=20, surface_azimuth=200,
                  module_parameters=dict(gamma_pdc=-0.003, pdc0=4500),
                  inverter_parameters=dict(pdc0=4000),
                  temperature_model_parameters=temperature_model_parameters)

mc = ModelChain.with_pvwatts(system, location)
weather = pd.DataFrame([[1050, 1000, 100, 30, 5], [500, 200, 10, 30, 5]],
                       columns=['ghi', 'dni', 'dhi', 'temp_air', 'wind_speed'],
                       index=[pd.Timestamp(x, tz='US/Arizona') for x in ['20170401 1200', '20170401 1300']])

mc.run_model(weather)
print(mc.results.dc)
print(mc.results.ac)

# Now with an array
module_name = 'SunPower SPR-210-WHT [ 2006]'
inverter_name = 'Power-One: PVI-CENTRAL-50-US [208V]'

modules = pvsystem.retrieve_sam('SandiaMod')
module_parameters = modules[normalize_product_names(module_name)]
inverters = pvsystem.retrieve_sam('cecinverter')
inverter_parameters = inverters[normalize_product_names(inverter_name)]

system = pvsystem.PVSystem(
    module_parameters=module_parameters,
    inverter_parameters=inverter_parameters,
    modules_per_string=20,
    strings_per_inverter=6,
    surface_tilt=10 + 53,
    surface_azimuth=120,
    albedo=0.8,
    temperature_model_parameters=temperature_model_parameters)

mc = ModelChain(system, location)
mc.run_model(weather)
print(mc.results.dc)
print(mc.results.ac)
