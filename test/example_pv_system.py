'''
Created on 21.05.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
# pick weather data
import os
import pandas as pd
from common import DATA_PROC_DIR, RESULTS_DIR, RESULTS_VIS
from configurations import generate_configurations, Scenarios
from photovoltaic import PVSystem
import matplotlib.pyplot as plt
import numpy as np
from src.data_processing.download_weather_data import write_csv_files
from os.path import join

SOLR_DATA = join(DATA_PROC_DIR, "SolarRadiation.csv")
TEMP_DATA = join(DATA_PROC_DIR, "Temperature.csv")
WIND_DATA = join(DATA_PROC_DIR, "WindSpeed.csv")


def check_data_weather(year=2016):
    '''
    Looks if weather data already exists.
    Downloads the data if the files do not exist.

    :param year: Year for data downloading
    '''
    download_data = False
    file_names = ["SolarRadiation.csv", "Temperature.csv", "WindSpeed.csv"]

    for f in file_names:
        if not os.path.isfile(os.path.join(DATA_PROC_DIR, f)):
            download_data = True

    if download_data:
        print("Dowloading weather data for year {}".format(year))
        write_csv_files(
            year=year,
            columns=[
                "SolarRadiation",
                "Temperature",
                "WindSpeed",
                "AirPressure"])
    else:
        print("Data found. Skipping download.")

    return None


def example_inverter_efficiency(days=1, calc_mode="detailed"):
    '''
    Calculates AC and DC production of a PV System in the BUILDING scenario and plots the information on energy production and inverter efficiency.
    Writes a CSV file with the results in  the directory ./data

    :param days: Number of days to perform calculations.
    :param calc_mode: Calculation Mode
    '''
    check_data_weather()
    solr_df = pd.read_csv(SOLR_DATA, skiprows=1, index_col=0)
    temp_df = pd.read_csv(TEMP_DATA, skiprows=1, index_col=0)
    wind_df = pd.read_csv(WIND_DATA, skiprows=1, index_col=0)
    weather = pd.concat([solr_df, temp_df, wind_df],
                        axis=1)
    week = 28
    weather = weather.tail(24 * 4 * (365 - 7 * week) - 1).head(24 * 4 * days)
    # generate pv systems
    config = generate_configurations(scenario=Scenarios.HOUSE)
    if calc_mode == "simple":
        # 8 Panels, 210W per panel, 2kW Inverter
        config["p_dc"] = 8 * 210
        config["p_ac"] = 2000
        config["inverter_eff"] = 0.96

    pv = pd.DataFrame(index=weather.index)

    power_dc = []
    power_ac = []
    eff = []
    # Create the PV Systems

    pv_sys = PVSystem(start_date="2020-06-12 12:00:00",
                      calc_mode=calc_mode, **config)

    for date in pv.index:
        pv_sys.set_date(date)
        pv_sys.power(
            ghi=weather.loc[date]["SolarRadiation"],
            temp_air=weather.loc[date]["Temperature"],
            wind_speed=weather.loc[date]["WindSpeed"]
        )
        power_ac.append(pv_sys.ac)
        power_dc.append(pv_sys.dc)
        eff.append(pv_sys.eff)

    # Process dataframes for calculations
    pv["PowerAC"] = power_ac
    pv["PowerDC"] = power_dc
    pv["InverterEff"] = eff
    pv["InverterEff"] = pv["InverterEff"].clip(lower=0).replace(0, np.nan)

    pv.index = pd.to_datetime(pv.index)
    pv.to_csv(os.path.join(RESULTS_DIR, f"ResultsBuilding-{calc_mode}.csv"))

    ax1 = pv[["PowerAC", "PowerDC"]].plot()
    ax1.set_title("AC and DC Power Production")
    ax1.set_ylabel("Power, W")
    plt.savefig(join(RESULTS_VIS, f"PowerProduced-{calc_mode}.svg"))
    ax2 = pv.plot.scatter(
        x="PowerDC",
        y="InverterEff",
        c="PowerAC",
        colormap="viridis")
    ax2.set_title("Inverter Efficiency")
    ax2.set_ylabel("Efficiency, 1")
    plt.savefig(join(RESULTS_VIS, f"InverterEfficiency-{calc_mode}.svg"))

    return pv


def example_different_configurations():
    '''
    Testing if the different configurations produce feasible results
    Weather data is the same for all locations
    '''
    location, power = [], []
    pvSys = PVSystem("2020-06-12 12:00:00")
    location.append("Default")
    power.append(pvSys.power(ghi=1000,
                             temp_air=20,
                             wind_speed=0,
                             ))

    configuration = generate_configurations(Scenarios.BASE)
    pvSys = PVSystem("2020-06-12 12:00:00", **configuration)
    location.append("OLD")
    power.append(pvSys.power(ghi=1000,
                             dni=950,
                             temp_air=20,
                             wind_speed=0,
                             ))

    configuration = generate_configurations(Scenarios.HOUSE)
    pvSys = PVSystem("2020-06-12 12:00:00", **configuration)
    location.append("Braunschweig")
    power.append(pvSys.power(ghi=1000,
                             dni=950,
                             temp_air=20,
                             wind_speed=0,
                             ))

    configuration = generate_configurations(Scenarios.BUILDING)
    pvSys = PVSystem("2020-06-12 12:00:00", **configuration)
    location.append("Berlin")
    power.append(pvSys.power(ghi=1000,
                             dni=950,
                             temp_air=5,
                             wind_speed=10,
                             pressure=101252
                             ))

    df = pd.DataFrame({'Scenario': location, "AC Power Output": power})
    ax = df.plot.barh(x='Scenario', y="AC Power Output")
    ax.set_xscale("log")
    ax.set_xlabel("AC Output Power, W")
    plt.savefig(join(RESULTS_VIS, "ScenarioProduction.svg"))


def example_changing_solar_angles():
    '''
    Test if the step_time() fuction of the PVSystem works.
    The only effect is changing the angle and thus the production.
    '''
    # Change of power due to different time.

    configuration = generate_configurations(Scenarios.BASE)
    pvSys = PVSystem("2020-06-12 06:00:00", **configuration)
    times, power = [], []
    for _ in range(40):
        pvSys.step_time(step_size=900)
        times.append(pvSys.datetime[0])
        power.append(pvSys.power())
    df = pd.DataFrame({"Time": times, "Power Output, W": power})
    df.set_index("Time", inplace=True)
    ax = df.plot()
    ax.set_title("Solar Angle Sensitivity")
    ax.set_ylabel("AC Output Power, W")
    ax.set_xlabel("Time")
    plt.savefig(join(RESULTS_VIS, "SolarAngleInfluence.svg"))


def main_test():
    '''
    Collection of all the tests
    '''

    example_inverter_efficiency(calc_mode="detailed", days=7)
    example_inverter_efficiency(calc_mode="simple", days=7)
    print("Done")
    # example_different_configurations()
    # example_changing_solar_angles()


if __name__ == '__main__':
    main_test()
    pass
