'''
Created on 14.12.2021

@author: Fernando Penaherrera @UOL/OFFIS

This tests assumes that the mosaik_pv is installed as package

Simple Mode Calculation

pip install mosaik-csv
pip install mosaik-hdf5
pip install git+ssh://git@gitlab.com/zdin-zle/models/photovoltaic.git

'''
# Configure Mosaik Components.
from common import DATA_PROC_DIR, RESULTS_DIR, RESULTS_VIS
from mosaik_csv import main
from configurations import Scenarios
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mosaik
import random
import h5py
from os.path import join
import logging

logging.basicConfig(level=logging.INFO)

SIM_CONFIG = {
    'CSV': {
        'python': 'mosaik_csv:CSV'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5',
    },
    'PVSim': {
        'python': 'src.mosaik_pv.photovoltaic_simulator:PVSimulator'
    },
}


# Simulation Parameters
START = '2016-01-01 00:00:00'  # Data in csv file are from 2016
END = 24 * 3600 * 7  # 1 Week
STEP_SIZE = 1 * 60 * 15
SCENARIO = Scenarios.BASE
CONFIG = {'albedo': 0.30,
          'elevation': 34 + 15,
          'inclination': 10,
          'latitude': 52.52,
          'longitude': 13.4,
          'surface_azimuth': 180,
          'timezone': 'Europe/Berlin',
          'p_ac': 50000,
          'p_dc': 120 * 210,
          'inverter_eff': 0.96,
          }

AIRP_DATA = join(DATA_PROC_DIR, 'AirPressure.csv')
SOLR_DATA = join(DATA_PROC_DIR, 'SolarRadiation.csv')
TEMP_DATA = join(DATA_PROC_DIR, 'Temperature.csv')
WIND_DATA = join(DATA_PROC_DIR, 'WindSpeed.csv')
DATA_BASE = join(RESULTS_DIR, 'test_results_simple.hdf5')


def create_scenario(world):
    '''
    Creation of the network of objects for the Co-Simulation

    :param world: mosaik.World object with the parameters of the simulation
    '''
    # Start Simulators
    # Load Data
    logging.info("Creating Components")
    solrData = world.start("CSV", sim_start=START, datafile=SOLR_DATA)
    airpData = world.start("CSV", sim_start=START, datafile=AIRP_DATA)
    tempData = world.start("CSV", sim_start=START, datafile=TEMP_DATA)
    windData = world.start("CSV", sim_start=START, datafile=WIND_DATA)

    # Load Components
    logging.info("Creating Models")
    pvSim = world.start("PVSim",
                        start_date=START,
                        step_size=STEP_SIZE,
                        calc_mode="simple")

    # Load Database
    db = world.start('DB', step_size=STEP_SIZE, duration=END)
    # Does the DB need to exists before?? No
    hdf5 = db.Database(filename=DATA_BASE)

    # Instantiate model entities based on their data sources
    # Output is a list with instances. For this example we need the first.
    solr = solrData.SolarRadiation.create(1)

    # This is rather clever so I can create 10 instances instead of 1....
    airp = airpData.AirPressure.create(1)
    temp = tempData.Temperature.create(1)
    wind = windData.WindSpeed.create(1)

    # pv entities
    pv = pvSim.PV.create(1, **CONFIG)

    # Connect entities
    # Data Entities to PV Entities.
    world.connect(solr[0], pv[0], ("SolarRadiation", 'GHI'))
    world.connect(airp[0], pv[0], ("AirPressure", 'pressure'))
    world.connect(temp[0], pv[0], ("Temperature", 'airTemp'))
    world.connect(wind[0], pv[0], ("WindSpeed", 'windSpeed'))

    # Connect Data and Results to the DB for writing
    world.connect(solr[0], hdf5, "SolarRadiation")
    world.connect(airp[0], hdf5, "AirPressure")
    world.connect(temp[0], hdf5, "Temperature")
    world.connect(wind[0], hdf5, "WindSpeed")
    world.connect(pv[0], hdf5, "p_gen")

    # A Web Visualization would be nice to have but unnecessary for testing
    # purposes
    return None


def coSimulation():
    '''
    Creation of the main instance for cosimulation
    '''
    random.seed(23)
    world = mosaik.World(SIM_CONFIG)
    create_scenario(world)
    world.run(until=END, print_progress=False)

    return None


# Results Analysis

def results_analysis():
    '''
    Reads the data stored in the HDF5 database,
    converts to a Data Frame and plot the results,
    and saves the results to a csv.
    '''
    logging.info("Analizing Database")

    data = h5py.File(DATA_BASE, "r")
    solar_rad = data["Series"]["CSV-0.SolarRadiation_0"]["SolarRadiation"]
    pv_output = data["Series"]["PVSim-0.PV_0"]["p_gen"]
    dti = pd.date_range(
        START,
        periods=len(
            np.array(pv_output)),
        freq="{}s".format(STEP_SIZE))
    results = {
        "Date": dti,
        "OutputPower": np.array(pv_output),
        "SolarRadiation": np.array(solar_rad)
    }
    df = pd.DataFrame(results)
    df.set_index("Date", inplace=True)
    df["OutputPower"] *= -1

    # Write dataframe to a CSV
    df.to_csv(join(RESULTS_DIR, "cosimulation_simple.csv"))
    logging.info(
        "Results written to {}".format(
            join(
                RESULTS_DIR,
                "cosimulation_simple.csv")))

    # Write a graph
    ax1 = df["OutputPower"].plot(label="Output Power")
    ax2 = df["SolarRadiation"].plot(secondary_y=True, style="y", label="GHI")
    ax1.set_title("Co-Simulation Results- Simple Mode")
    ax1.set_ylabel('Output Power')
    ax2.set_ylabel('Solar Radiation')
    ax1.legend(loc="upper left")
    ax2.legend(loc="upper right")
    plt.savefig(join(RESULTS_VIS, "cosimulation_simple.svg"))
    logging.info(
        "Plot saved to {}".format(
            join(
                RESULTS_VIS,
                "cosimulation_simple.svg")))


def main():
    coSimulation()
    results_analysis()


if __name__ == '__main__':
    main()
    plt.show()
