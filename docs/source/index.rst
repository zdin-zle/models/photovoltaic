.. Mosaik-PV documentation master file, created by
   sphinx-quickstart on Mon Jun  7 16:14:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mosaik-PV's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   readme
   includelicense
   modules
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
