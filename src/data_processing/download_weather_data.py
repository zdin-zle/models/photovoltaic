'''
Created on 18.05.2021

@author: Fernando Penaherrera

Downloads weather data from the Uni Oldenburg Website.
The data is open and free to use. This is to gather an example
of weather and test the compatibility

'''
from src.mosaik_pv.common import DATA_PROC_DIR, DATA_RAW_DIR
import urllib.request as rq
import os.path
import pandas as pd
from src.mosaik_pv.common import convert_german_date


BASE_URL = "https://uol.de/dez4/wetter/ausgabe.php?datei="
# From the UOL Website, these are the columns with the relevant headings
HEADINGS = {
    0: "Date",
    1: "Time",
    2: "WindSpeed",
    3: "Temperature",
    8: "SolarRadiation",
    10: "AirPressure"
}


def download_data_year(year=2017):
    '''
    Downloads all TXT files for wheater data fron the UOL website.
    Currently bypassed

    :param year: Year for looking up data
    '''
    year = str(year)
    for month in range(1, 13):
        filename = "wetter{}{}.txt".format(year[2:4], str(month).zfill(2))
        full_filename = DATA_RAW_DIR + "\\" + filename
        if not os.path.isfile(full_filename):
            WETTER_DATA = BASE_URL + filename
            rq.urlretrieve(WETTER_DATA, full_filename)
            print("File written : " + full_filename)
    return None


def get_weather_df(year=2017, save=False):
    '''
    Downloads and converts directly the data from the UOL website into a dataframe
    with the appropiate coulums in the HEADER dictionary.

    :param year: int of the year. Between 2015 and 2019.

    :return: Dataframe with the weather information
    '''
    if year not in range(2015, 2020):
        raise ValueError('Year has to be an integer beetwen 2015 and 2019')

    year = str(year)
    df_list = []

    for month in range(1, 13):
        filename = "wetter{}{}.txt".format(year[2:4], str(month).zfill(2))

        # Download and converd directly the data to a dataframe
        WETTER_DATA = BASE_URL + filename
        wetter_df = pd.read_csv(WETTER_DATA, delimiter="\t",
                                header=None, decimal=",")

        columns = [k for k in HEADINGS.keys()]

        # Filter only the data needed. Columns 0, 1, 2, 3, 8, 10
        wetter_df = wetter_df[columns]
        wetter_df.columns = [v for v in HEADINGS.values()]

        # Fix the daytime values
        wetter_df["Date"] = wetter_df["Date"].apply(
            lambda x: convert_german_date(x))
        wetter_df["DateTimeStr"] = wetter_df["Date"]\
            + " " + wetter_df["Time"]
        wetter_df["DateTime"] = pd.to_datetime(
            wetter_df["DateTimeStr"], format='%Y-%m-%d %H:%M:%S')
        wetter_df.drop(["Date", "Time", "DateTimeStr"],
                       axis=1, inplace=True)
        wetter_df.rename(columns={"DateTime": "Date"}, inplace=True)
        wetter_df.set_index("Date", inplace=True)

        wetter_df["AirPressure"] *= 100
        # Append to the list of dataframes
        df_list.append(wetter_df)

    # Concatenate the dataframes
    wetter = pd.concat(df_list)

    # Some data is duplicated
    wetter = wetter[~wetter.index.duplicated(keep='first')]

    # Re-sample the values
    wetter = wetter.resample("15T").pad()
    wetter.sort_index()

    # Save if specified
    if save:
        full_filename = os.path.join(
            DATA_PROC_DIR, "weather{}.csv".format(year))
        wetter.to_csv(full_filename)
        print("File written: " + full_filename)
    return wetter


def write_csv_files(year=2017, columns=["SolarRadiation"]):
    '''
    Writes a csv of the column of a dataframe
    The output files need to be in the MOSAIK-CSV format::

        SolarRadiation
        Date                    I # [W/m2]
        01.01.2014 00:00        0
        01.01.2014 00:01        0
        ...


    Files are written in the ./data/processed folder.

    :param year: int of the required year
    :param columns: List of values to be written. One of ["WindSpeed", "Temperature", "SolarRadiation", "AirPressure"]
    '''

    if isinstance(columns, str):
        columns = [columns]

    for c in columns:
        if c not in [
            "WindSpeed",
            "Temperature",
            "SolarRadiation",
                "AirPressure"]:
            raise ValueError(
                'Input column has to be one of of: \n "WindSpeed", "Temperature", "SolarRadiation", "AirPressure" ')

    weather_df = get_weather_df(year)
    weather_df = weather_df[columns]

    for c in columns:
        data = weather_df[c]
        header = c.replace(" ", "")  # Remove Spaces
        filename = os.path.join(DATA_PROC_DIR, header + ".csv")

        # Write the CSV file of with the column of data
        data.to_csv(filename)

        # Write the header so that it is readable with MOSAIK-CSV
        with open(filename, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(header + '\n' + content)
            print("File written: {}".format(filename))


if __name__ == '__main__':

    download_data_year(2017)
    get_weather_df(2019, save=True)
    write_csv_files(
        year=2016,
        columns=[
            "SolarRadiation",
            "Temperature",
            "WindSpeed",
            "AirPressure"])
