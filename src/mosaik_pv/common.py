'''
Created on 03.06.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''

import os
from os.path import join



def get_project_root():
    """
    Returns the path to the project root directory.

    :return: A directory path.
    :rtype: str
    """
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir, os.pardir
    ))


BASE_DIR = get_project_root()
DATA_DIR = join(BASE_DIR,"data")
DATA_RAW_DIR = join(DATA_DIR,"raw")
DATA_PROC_DIR = join(DATA_DIR,"processed")
SRC_DIR = join(BASE_DIR,"src")
SRC_DATA_DIR = join(SRC_DIR , "data")
SRC_MODEL_DIR = join(SRC_DIR ,"model")
RESULTS_DIR = join(BASE_DIR,"results","data")
RESULTS_VIS = join(BASE_DIR, "results","visualization")
TEST_DIR = join(BASE_DIR,"test")
DOCS_DIR = join(BASE_DIR,"docs")


def pretty_print(d, indent=0):
    '''
    Nested function for easy printing of a dictionary.
    Auxiliar function to evaluate a system at a given point of time.

    :param d: Dictionary with the operational parameteres
    :param indent: Desired indentation. 0 for initial indentation.
    '''

    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty_print(value, indent + 1)
        else:
            try:
                print('\t' * (indent + 1) + str(round(value, 1)))
            except BaseException:
                print('\t' * (indent + 1) + str(value))


def convert_german_date(date="30.12.2021"):
    '''
    Convert the date from the German format to the standard format with dashes

    :param date: Date in the format DD.MM.YYYY

    :return: Date in the format YYYY-MM-DD
    :rtype: str

    '''
    return (date[6:] + "-" + date[3:5] + "-" + date[0:2])


def normalize_product_names(name):
    '''
    Normalize the names to exclude special characters
    Useful to look names from the PVLib and pass them into the dictionaries

    :param name: Str Name of the inverter or the module

    :return: Name with replaced characters
    :rtype: str
    '''
    BAD_CHARS = ' -.()[]:+/",'
    GOOD_CHARS = '____________'

    mapping = str.maketrans(BAD_CHARS, GOOD_CHARS)
    norm_name = name.translate(mapping)
    return norm_name


if __name__ == '__main__':
    pass
