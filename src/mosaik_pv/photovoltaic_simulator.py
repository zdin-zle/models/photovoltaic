'''
Created on 19.05.2021

@author: Fernando Penaherrera

Adapter for the MOSAIK-API for connection of the PVSystem of the PV System

TODO: Implement Raise value errors

'''

import itertools
import mosaik_api
from .photovoltaic import PVSystem
from .configurations import Scenarios, generate_configurations

meta = {
    'type': 'time-based',
    'models': {
        'PV': {
            'public': True,
            # each of the parameters can be manually configured as well
            'params': list(generate_configurations().keys()),
            'attrs': ['p_gen',      # output active power [W]
                      'GHI',        # input direct normal irradiation [W/m2]
                      'windSpeed',  # input Wind Speed [W/m2]
                      'airTemp',    # input Air Temperature Speed [C]
                      'pressure',   # input Air Pressure [Pa]. Can be None
                      ]
        },
    },
}

DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'


class PVSimulator(mosaik_api.Simulator):
    '''
    Simulator to be coupled in the co-simulation framework.
    '''

    def __init__(self):
        # all methods in the inheritance chain are called.
        super(PVSimulator, self).__init__(meta)

        self.gen_neg = False     # true if generation is negative for coupling with PandaPower

        self.sid = None
        self.cache = None

        # Dictionaries for handling
        self._entities = {}
        self.eid_counters = {}

    def init(self,
             sid,  # Which Format?
             time_resolution = 60, # check the time resolution
             start_date=None,  # Which format??
             step_size=60,  # seconds
             gen_neg=True, 
             calc_mode="detailed",
             ):

        self.sid = sid
        self.gen_neg = gen_neg
        self.time_resolution = time_resolution
        self.start_date = start_date
        self.step_size = step_size
        self.calc_mode = calc_mode

        # for testing
        return self.meta

    def create(self, num, model,  **model_params):
        # This is the Value to be returned in case PVSystem is not found.
        counter = self.eid_counters.setdefault(model, itertools.count())
        
        entities = []
        if model_params:
            self.model_params = model_params

        # creation of the entities:
        for _ in range(num):
            eid = '{}_{}'.format(model, next(counter))  # Entities IDs
            
            self._entities[eid] = PVSystem(start_date=self.start_date,
                                           calc_mode=self.calc_mode,
                                           **model_params)
                
            entities.append({'eid': eid, 'type': model, 'rel': []})

        return entities

    def step(self, t, inputs, max_advance=3600):
        self.cache = {}
        for eid, attrs in inputs.items():  

            for attr, vals in attrs.items():
                pressure = None
                dni= None 
                dhi = None
                windSpeed = None
                airTemp = None
                if attr == 'GHI':
                    ghi = list(vals.values())[0]
                
                if attr == 'DNI':
                    dni = list(vals.values())[0]

                if attr == 'DHI':
                    dhi = list(vals.values())[0]

                if attr == 'windSpeed':
                    windSpeed = list(vals.values())[0]

                if attr == 'airTemp':
                    airTemp = list(vals.values())[0]

                if attr == 'pressure':
                    pressure = list(vals.values())[0]
            
        
            self.cache[eid] = self._entities[eid].power(ghi=ghi,
                                                        dni=dni,
                                                        dhi=dhi,
                                                        temp_air=airTemp,
                                                        wind_speed=windSpeed,
                                                        pressure=pressure
                                                        )
            self._entities[eid].step_time(self.step_size)
            
            if self.gen_neg:
                self.cache[eid] *= (-1)

        return t + self.step_size

    def get_data(self, outputs):
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self._entities.keys():
                raise ValueError('Unknown entity ID "{}"'.format(eid))

            data[eid] = {}
            for attr in attrs:
                if attr != 'p_gen':
                    raise ValueError(
                        'Unknown output attribute "{}"'.format(attr))
                data[eid][attr] = self.cache[eid]

        return data


if __name__ == '__main__':
    scenario = Scenarios.BASE
    config = generate_configurations(scenario)
    pvSim = PVSimulator()

    pvSim.init(sid=1,
               start_date="2020-01-01 12:00:00",
               step_size=90,
               gen_neg=True)

    e = pvSim.create(num=2, PVSystem=PVSystem, **config)


    pass
