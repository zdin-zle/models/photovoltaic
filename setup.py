'''
Created on 01.06.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''

"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils

from setuptools import setup, find_packages
import pathlib
here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='mosaik-pv',  # Required
    version='0.2.1',  # Required
    description='A mosaik adapter for a PV Model based on the PVLib package',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://gitlab.com/zdin-zle/models/photovoltaic',  # Optional
    author='Fernando Penaherrera V.',  # Optional
    author_email='fernandoandres.penaherreravaca@offis.de',  # Optional
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Researchers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',

    ],

    keywords='Photovoltaic, Mosaik, Adapter',  # Optional
    packages=find_packages(
        where="src",
        include=["mosaik*"]

    ),
    package_dir={"": "src"},
    python_requires='>=3.6, <4',
    install_requires=[
        "arrow>=1.1.0",
        "mosaik>=3.0.0",
        "pvlib==0.9.0"],

    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/zdin-zle/models/photovoltaic/-/issues',
        'Funding': 'ZLE Funders',
        'Documentation': "https://zdin.de/zukunftslabore/energie",
        'Source': "https://gitlab.com/zdin-zle/models/photovoltaic",
    },
)


if __name__ == '__main__':
    pass
