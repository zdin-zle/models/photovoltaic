'''
Created on 15.12.2021

@author: Fernando Penaherrera @UOL/OFFIS

A main class referencing the examples

'''
from test.example_pv_adapter_simple import main as main_simple
from test.example_pv_adapter_detailed import main as main_detailed

if __name__ == '__main__':
    main_simple()
    main_detailed()
    
    
    